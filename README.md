# MD TG 2.0

Compliance of SK metadata with Technical Guidance for the implementation of INSPIRE dataset and service metadata based on ISO/TS 19139:2007
https://inspire.ec.europa.eu/id/document/tg/metadata-iso19139

Compliance of SK metadata with Technical Guidance v 2.0 has been supported via on the fly XSLT trasformation at [SK National discovery CSW service](https://rpi.gov.sk/rpi_csw/service.svc/get?request=GetCapabilities&service=CSW&version=2.0.2). CSW service use [XSLT transformation file](https://gitlab.com/mzpsr/mzpsr/podpora-inspire-implement-cie/meta-daje/md-tg-2.0/-/blob/master/INSPIRE_TG2.xslt) during operation GetRecords and provides on the fly XSLT transformation of metadata to encoding required by Technical Guidance v 2.0. That means, in the database there are for time being (till the new RPI 2.0 development will be finished) still records encoded by Technical Guidance v 1.3 requirements and only output of CSW is encoded according TG v 2.0 requirements.

Acknowledgements: 
- Rado Chudý (Sevitech)
- Antonio Rotundo ([AgID/rndt-md-converter](https://github.com/AgID/rndt-md-converter)) 
 
